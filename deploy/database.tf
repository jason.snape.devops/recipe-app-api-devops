resource "aws_db_subnet_group" "main" {
  name       = "${local.prefix}-main"
  subnet_ids = [aws_subnet.private_a.id, aws_subnet.private_b.id]

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}

resource "aws_security_group" "rds" {
  name        = "${local.prefix}-rds-inbound-access"
  description = "Allow access to postgres DB"
  vpc_id      = aws_vpc.main.id

  ingress {
    description = "Access to DB"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
  }

  tags = local.common_tags
}

resource "aws_db_instance" "main" {
  identifier              = "${local.prefix}-db"
  allocated_storage       = 20
  storage_type            = "gp2"
  db_subnet_group_name    = aws_db_subnet_group.main.name
  vpc_security_group_ids  = [aws_security_group.rds.id]
  engine                  = "postgres"
  engine_version          = "11.15"
  instance_class          = "db.t2.micro"
  name                    = "recipe"
  backup_retention_period = 0
  multi_az                = false
  username                = var.db_password
  password                = var.db_username
  skip_final_snapshot     = true

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}