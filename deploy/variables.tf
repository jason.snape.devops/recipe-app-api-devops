
variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-api-app-devops"
}

variable "contact" {
  default = "test@app.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}
