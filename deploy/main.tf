terraform {
  backend "s3" {
    bucket         = "recipe-app-api-devops-jason-tfstate"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  version = "~> 2.54.0"
  region  = "us-east-1"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    Managedby   = "Terraform"
  }
}

data "aws_region" "current" {}

